// +build go1.13

package signal

import "errors"

func errorsIs(err, target error) bool {
	return errors.Is(err, target)
}
