// +build !go1.13

package signal

import "golang.org/x/xerrors"

func errorsIs(err, target error) bool {
	return xerrors.Is(err, target)
}
