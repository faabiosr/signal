package signal

import (
	"context"
	"errors"
	"os"
	"os/signal"
	"sync"
)

type watcherCtx struct {
	context.Context
	done      chan struct{}
	signals   chan os.Signal
	mu        sync.Mutex
	err       error
	finalized bool
}

var _ context.Context = &watcherCtx{}

func (c *watcherCtx) Done() <-chan struct{} { return c.done }

func (c *watcherCtx) Err() error {
	c.mu.Lock()
	defer c.mu.Unlock()
	return c.err
}

func (c *watcherCtx) watch() {
	select {
	case sig := <-c.signals:
		c.finish(errors.New("canceled by signal: " + sig.String()))
	case <-c.Context.Done():
		c.finish(c.Context.Err())
	}
}

func (c *watcherCtx) cancel() {
	c.finish(context.Canceled)
}

func (c *watcherCtx) finish(err error) {
	c.mu.Lock()
	defer c.mu.Unlock()
	if c.finalized {
		return
	}
	signal.Stop(c.signals)
	c.err = err
	close(c.done)
	c.finalized = true
}

// NewWatcher returns a new context that will cancel when any of sig are
// received.
func NewWatcher(ctx context.Context, sig ...os.Signal) (context.Context, context.CancelFunc) {
	c := &watcherCtx{
		Context: ctx,
		signals: make(chan os.Signal),
		done:    make(chan struct{}),
	}
	signal.Notify(c.signals, sig...)
	go c.watch()
	return c, c.cancel
}
