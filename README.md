# signal

This package provides a simple wrapper around signal.Notify and a context.Context.

# Compatibility

This package doesn't use any special features, so should work with any version of Go with context support. But it is only tested automatically against 1.11 and newer, because these versions support modules.

# License

This software is released under the terms of the MIT license.
