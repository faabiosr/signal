module gitlab.com/flimzy/signal

go 1.13

require (
	gitlab.com/flimzy/testy v0.0.3
	golang.org/x/xerrors v0.0.0-20191204190536-9bdfabe68543
)
