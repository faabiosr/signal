package signal

import (
	"context"
	"os"
	"testing"

	"gitlab.com/flimzy/testy"
)

func TestWatch(t *testing.T) {
	interrupted := "canceled by signal: interrupt"
	t.Run("signal", func(t *testing.T) {
		c := &watcherCtx{
			Context: context.Background(),
			signals: make(chan os.Signal),
			done:    make(chan struct{}),
		}
		go c.watch()
		c.signals <- os.Interrupt
		<-c.Done()
		err := c.Err()
		testy.Error(t, interrupted, err)
	})
	t.Run("cancel", func(t *testing.T) {
		c := &watcherCtx{
			Context: context.Background(),
			signals: make(chan os.Signal),
			done:    make(chan struct{}),
		}
		go c.watch()
		c.cancel()
		<-c.Done()
		err := c.Err()
		testy.Error(t, "context canceled", err)
	})
	t.Run("NewWatcher", func(t *testing.T) {
		// This tests basically everything except the signal.Notify() call
		ctx, cancel := NewWatcher(context.Background(), os.Interrupt)
		defer cancel()
		c := ctx.(*watcherCtx)
		c.signals <- os.Interrupt
		<-c.Done()
		err := c.Err()
		testy.Error(t, interrupted, err)
	})
	t.Run("finalized", func(t *testing.T) {
		ctx, cancel := NewWatcher(context.Background(), os.Interrupt)
		c := ctx.(*watcherCtx)
		c.signals <- os.Interrupt
		<-c.Done()
		cancel() // Make sure this doesn't panic for closing closed chan
		err := c.Err()
		testy.Error(t, interrupted, err)
	})
	t.Run("original context cancelled", func(t *testing.T) {
		ctx, cancel := context.WithCancel(context.Background())
		cancel()
		c, cancel := NewWatcher(ctx, os.Interrupt)
		defer cancel()
		<-c.Done()
		err := c.Err()
		testy.Error(t, "context canceled", err)
	})
}
